from django.urls import path

from shop.views import (ProductListView, OrderListView,
                        ProductDetailsView, BasketDetailsView,
                        DeleteProductFromBasket,
                        OrderDetailsView, OrderNewView)


urlpatterns = [
    path('products/', ProductListView.as_view(), name='product_list'),
    path('products/<int:pk>/', ProductDetailsView.as_view(), name='product_details'),
    path('basket/', BasketDetailsView.as_view(), name='basket'),
    path('basket/delete/<int:pk>/', DeleteProductFromBasket.as_view(), name='basket_remove'),
    path('orders/', OrderListView.as_view(), name='order_list'),
    path('orders/new/', OrderNewView.as_view(), name='order_new'),
    path('orders/<int:pk>/', OrderDetailsView.as_view(), name='order_details'),
]

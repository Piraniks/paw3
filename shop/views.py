from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.core.paginator import Paginator
from django.db.transaction import atomic
from django.shortcuts import render, redirect, reverse, get_object_or_404
from django.utils.decorators import method_decorator
from django.views import View

from shop.models import Product, Basket, BasketProduct, Order


class ProductListView(View):
    def get(self, request):
        _products = Product.objects.all()

        paginator = Paginator(_products, 8)
        page = request.GET.get('page')
        products = paginator.get_page(page)

        context = {'products': products}
        return render(request, 'shop/product_list.html', context=context)


class ProductDetailsView(View):
    def get(self, request, pk):
        product = get_object_or_404(Product, pk=pk)
        context = {'product': product}
        return render(request, 'shop/product_details.html', context=context)


class BasketDetailsView(View):
    @method_decorator(login_required)
    def get(self, request):
        basket, _ = Basket.objects.get_or_create(owner=request.user,
                                                 has_been_ordered=False)

        basket_products = BasketProduct.objects.filter(basket=basket)

        context = {'basket': basket, 'basket_products': basket_products}
        return render(request, 'shop/basket.html', context=context)

    @method_decorator(login_required)
    def post(self, request):
        product_pk = request.POST.get('pk')

        try:
            quantity = int(request.POST.get('quantity'))
            if quantity < 1:
                raise ValueError

        except ValueError:
            messages.add_message(request, messages.ERROR,
                                 'Quantity must be a positive integer.')
            return redirect(request.META.get('HTTP_REFERER'))

        basket, _ = Basket.objects.get_or_create(owner=request.user,
                                                 has_been_ordered=False)

        product = get_object_or_404(Product, pk=product_pk)

        if BasketProduct.objects.filter(basket=basket, product=product).exists():
            messages.add_message(request, messages.ERROR,
                                 'Product already in basket.')
            return redirect(request.META.get('HTTP_REFERER'))

        BasketProduct.objects.create(basket=basket, product=product,
                                     quantity=quantity)

        return redirect(reverse('basket'))


class DeleteProductFromBasket(View):
    @method_decorator(login_required)
    def post(self, request, pk):
        basket, _ = Basket.objects.get_or_create(owner=request.user,
                                                 has_been_ordered=False)

        product = get_object_or_404(Product, pk=pk)

        BasketProduct.objects.filter(basket=basket, product=product).delete()

        return redirect(reverse('basket'))


class OrderListView(View):
    @method_decorator(login_required)
    def get(self, request):
        _orders = Order.objects.filter(owner=request.user)

        paginator = Paginator(_orders, 8)
        page = request.GET.get('page')
        orders = paginator.get_page(page)

        context = {'orders': orders}
        return render(request, 'shop/order_list.html', context=context)


class OrderDetailsView(View):
    @method_decorator(login_required)
    def get(self, request, pk):
        order = get_object_or_404(Order, pk=pk, owner=request.user)

        context = {'order': order}

        return render(request, 'shop/order_details.html', context=context)


class OrderNewView(View):
    @method_decorator(login_required)
    def post(self, request):
        basket, _ = Basket.objects.get_or_create(owner=request.user,
                                                 has_been_ordered=False)

        if basket.is_empty:
            messages.add_message(request, messages.ERROR,
                                 'Cannot order empty basket.')
            return redirect(request.META.get('HTTP_REFERER'))

        with atomic():
            order = Order.objects.create(basket=basket, owner=request.user,
                                         value=basket.value)

            basket.has_been_ordered = True
            basket.save()

        return redirect(reverse('order_details', kwargs={'pk': order.pk}))

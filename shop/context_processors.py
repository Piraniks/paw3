from django.db.models import Sum

from shop.models import Basket, BasketProduct


def basket_context(request):
    """
    Adds basket context.
    """
    basket_quantity = 0
    basket_value = 0
    if request.user.is_authenticated:
        basket, _ = Basket.objects.get_or_create(owner=request.user,
                                                 has_been_ordered=False)

        basket_quantity_data = BasketProduct.objects.filter(basket=basket).aggregate(Sum('quantity'))
        basket_quantity = basket_quantity_data.get('quantity__sum') or 0
        basket_value = basket.value

    context = {
        'basket_quantity': basket_quantity,
        'basket_value': basket_value
    }

    return context

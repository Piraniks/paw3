from django.contrib import admin


from shop.models import Basket, BasketProduct, Order, Product


admin.site.register(Basket)
admin.site.register(BasketProduct)
admin.site.register(Order)
admin.site.register(Product)

from django.db import models
from django.contrib.auth.models import User


class Product(models.Model):
    name = models.CharField(max_length=100, unique=True)
    description = models.TextField()
    value = models.DecimalField(max_digits=10, decimal_places=2)

    class Meta:
        ordering = ['-id']

    def __str__(self):
        return self.name


class Basket(models.Model):
    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name='baskets')
    products = models.ManyToManyField(Product, through='BasketProduct')

    timestamp = models.DateTimeField(auto_now_add=True)
    has_been_ordered = models.BooleanField(default=False)

    @property
    def value(self):
        basket_products = BasketProduct.objects.filter(basket=self)
        return sum(basket_product.value for basket_product in basket_products)

    @property
    def is_empty(self):
        return not self.products.exists()


class BasketProduct(models.Model):
    basket = models.ForeignKey(Basket, on_delete=models.CASCADE, related_name='baskets')
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='products')

    quantity = models.PositiveIntegerField(default=1)

    class Meta:
        ordering = ['-id']
        unique_together = ('product', 'basket')

    @property
    def value(self):
        return self.product.value * self.quantity


class Order(models.Model):
    PENDING = 1
    IN_PROGRESS = 2
    DONE = 3
    CANCELED = 0
    STATUS_CHOICES = (
        (PENDING, 'PENDING'),
        (IN_PROGRESS, 'IN_PROGRESS'),
        (DONE, 'DONE'),
        (CANCELED, 'CANCELED'),
    )

    timestamp = models.DateTimeField(auto_now_add=True)
    status = models.PositiveSmallIntegerField(choices=STATUS_CHOICES, default=PENDING)
    value = models.DecimalField(max_digits=10, decimal_places=2)

    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name='orders')
    basket = models.OneToOneField(Basket, on_delete=models.CASCADE, related_name='orders')

    class Meta:
        ordering = ['-id']

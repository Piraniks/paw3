from django.shortcuts import render, render_to_response
from django.views import View



def handler404(request, *args, **argv):
    response = render_to_response('404.html')
    response.status_code = 404
    return response


def handler403(request, *args, **argv):
    response = render_to_response('403.html')
    response.status_code = 500
    return response


class IndexView(View):
    def get(self, request):
        return render(request, 'index.html')

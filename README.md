# PAW project.

`Python3.7+` is requires, other requirements are listed in `requirements.txt`.

To install dependencies:

```
python -m pip install -r requirements.txt
```

To migrate database:

```
python manage.py migrate
```

To run:

```
python manage.py runsslserver localhost:9875
```